package com.bigfinscientific.bigfinbletest

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bigfinscientific.bigfinbletest.ble.gatt.BluetoothGATTListener
import com.bigfinscientific.bigfinbletest.ble.gatt.BluetoothGATTManager
import com.bigfinscientific.bigfinbletest.ble.gatt.BluetoothGattIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class MainActivity : AppCompatActivity(), BluetoothGATTListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bondedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        when {
            bondedDevices.size > 1 -> {
                Toast.makeText(this, "Please only connect one device at a time!", Toast.LENGTH_LONG)
                    .show()
            }
            bondedDevices.isEmpty() -> {
                Toast.makeText(this, "No paired device found!", Toast.LENGTH_LONG).show()
            }
            else -> {
                val deviceId: String = bondedDevices.elementAt(0).address
                BluetoothGATTManager.connect(this, this, deviceId)
            }
        }
    }

    override fun onPause() {
        BluetoothGATTManager.closeConnection()
        super.onPause()
    }

    override fun onDeviceConnected() {
        Util.Logger("Connected Established")
    }

    override fun onDeviceDisconnected() {
        Util.Logger("Connection Disconnected")
    }

    override fun onDeviceConnectionError() {
        Util.Logger("Error connecting to device")
    }

    override fun onServiceFound(service: String) {
        if (service == SYLVAC_SERVICE_UUID.toString()) {
            GlobalScope.launch(Dispatchers.IO) {
                BluetoothGattIO(BluetoothGATTManager.gattClient).enableNotifications(
                    SYLVAC_SERVICE_UUID,
                    SYLVAC_ANSWER_FROM_INSTRUMENT_UUID
                )

                Thread.sleep(1500)

                BluetoothGattIO(BluetoothGATTManager.gattClient).enableIndications(
                    SYLVAC_SERVICE_UUID,
                    SYLVAC_RECEIVED_DATA_UUID
                )
            }
        }
    }
}
