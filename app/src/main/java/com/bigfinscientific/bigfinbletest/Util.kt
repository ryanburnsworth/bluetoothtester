package com.bigfinscientific.bigfinbletest

import android.util.Log
import java.util.*

class Util {
    companion object {
        fun convertFromInteger(i: Int): UUID? {
            val MSB = 0x0000000000001000L
            val LSB = -0x7fffff7fa064cb05L
            val value = (i and ((-0x1).toLong()).toInt()).toLong()
            return UUID(MSB or (value shl 32), LSB)
        }

        fun Logger(message: String) {
            Log.e("BFS_BluetoothTest", message)
        }
    }
}
