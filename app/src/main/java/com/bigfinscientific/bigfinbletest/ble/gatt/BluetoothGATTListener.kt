package com.bigfinscientific.bigfinbletest.ble.gatt

interface BluetoothGATTListener {
    fun onDeviceConnected()
    fun onDeviceDisconnected()
    fun onDeviceConnectionError()
    fun onServiceFound(service: String)
}
