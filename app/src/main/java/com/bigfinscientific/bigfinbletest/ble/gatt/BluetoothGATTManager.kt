package com.bigfinscientific.bigfinbletest.ble.gatt

import android.annotation.SuppressLint
import android.bluetooth.*
import android.content.Context
import android.util.Log
import com.bigfinscientific.bigfinbletest.Util
import org.jetbrains.anko.doAsync
import java.util.*
import kotlin.concurrent.schedule

@SuppressLint("StaticFieldLeak")
object BluetoothGATTManager {
    private lateinit var bluetoothGATTListener: BluetoothGATTListener
    var gattClient: BluetoothGatt? = null
    private lateinit var device: BluetoothDevice
    private lateinit var context: Context

    fun connect(context: Context, bluetoothGATTListener: BluetoothGATTListener, deviceId: String) {
        Util.Logger("Attempting to connect to $deviceId")
        BluetoothGATTManager.bluetoothGATTListener = bluetoothGATTListener
        device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceId)
        BluetoothGATTManager.context = context
        createGATTConnection()
    }

    private fun createGATTConnection() {
        doAsync {
            gattClient = device.connectGatt(
                context,
                false,
                bleGattCallback,
                BluetoothDevice.TRANSPORT_AUTO
            )
        }
    }

    fun closeConnection() {
        Util.Logger("Closing Connection")
        bluetoothGATTListener.onDeviceDisconnected()
        gattClient?.run {
            disconnect()
            close()
        }
    }

    private val bleGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (newState != BluetoothGatt.STATE_CONNECTED) {
                bluetoothGATTListener.onDeviceConnectionError()
            } else {
                Util.Logger("Connection established. Discovering services...")
                Timer().schedule(1000) { gatt?.discoverServices() }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            gatt?.services?.forEach { it ->
                Util.Logger("Service Found: ${it.uuid}")
                bluetoothGATTListener.onServiceFound(it.uuid.toString())
                it.characteristics.forEach { c ->
                    Util.Logger("-- Characteristic: ${c.uuid}")
                }
            }
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            Util.Logger("Incoming Data: ${characteristic?.value}")
        }
    }
}
