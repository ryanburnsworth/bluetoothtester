package com.bigfinscientific.bigfinbletest.ble.gatt

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattDescriptor
import android.util.Log
import com.bigfinscientific.bigfinbletest.SYLVAC_CCCD_UUID
import com.bigfinscientific.bigfinbletest.Util
import java.util.*

class BluetoothGattIO(private val gatt: BluetoothGatt?) {
    fun enableNotifications(serviceUUID: UUID, characteristicUUID: UUID) {
        val service = gatt?.getService(serviceUUID)
        val characteristic = service?.getCharacteristic(characteristicUUID)
        gatt?.setCharacteristicNotification(characteristic, true)

        val descriptor = characteristic?.getDescriptor(SYLVAC_CCCD_UUID)
        descriptor?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        Util.Logger("Enabling Notifications")

        if (gatt?.writeDescriptor(descriptor) == true)
            Util.Logger("Notifications Enabled!")
        else
            Util.Logger("Enabling Notifications FAILED!")
    }

    fun enableIndications(serviceUUID: UUID, characteristicUUID: UUID) {
        val service = gatt?.getService(serviceUUID)
        val characteristic = service?.getCharacteristic(characteristicUUID)
        gatt?.setCharacteristicNotification(characteristic, true)

        val descriptor = characteristic?.getDescriptor(SYLVAC_CCCD_UUID)
        descriptor?.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        Util.Logger("Enabling Indications")

        if (gatt?.writeDescriptor(descriptor) == true)
            Util.Logger("Indications Enabled!")
        else
            Util.Logger("Enabling Indications FAILED!")
    }
}
