package com.bigfinscientific.bigfinbletest.data

import java.util.*
import kotlin.collections.ArrayList

class Device {
    var name: String = ""
    var address: String = ""
    var status: Int = 0
    var type: Int = 0
    var UUIDs: ArrayList<UUID> = arrayListOf()
}
